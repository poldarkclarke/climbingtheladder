﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp1
{
    public class ClimbingTheLadder
    {
        public static List<int> ClimbingLeaderboard(List<int> ranked, List<int> player)
        {
            List<int> scoreBoardPositions = new List<int>();

            // we can remove duplicate high scores as we are using Denis ranking
            List<int> distinctRankedScores = ranked.Distinct().ToList();
            var enumeratorPlayerScore = player.GetEnumerator();

            // move through player scores.
            var iscoreboardCount = distinctRankedScores.Count;
            var iScoreBoardPosition = iscoreboardCount - 1;
            //default to last place
            var refPosition = iscoreboardCount + 1;
            
            // List position starts from 0 so 0 is 1st etc..
            // As a result you can see me add 1 or 2 to value

            while (enumeratorPlayerScore.MoveNext())
            {
                var playerScore = (int)enumeratorPlayerScore.Current;

                bool moved = false;
                // If player score is greater find when it's not...
                while (playerScore > distinctRankedScores[iScoreBoardPosition] && iScoreBoardPosition > 0)
                {
                    iScoreBoardPosition--;
                    moved = true;
                }

                if (moved)
                {
                    // if we bettered the previous score we go above.. else we stay the same
                    if (iScoreBoardPosition == 0)
                    {
                        if (playerScore >= distinctRankedScores[iScoreBoardPosition])
                        {
                            //top
                            refPosition = iScoreBoardPosition + 1; 
                        } 
                        else
                        {
                            //moved so must be better then iScoreBoardPosition.
                            refPosition = iScoreBoardPosition + 2;
                        }
                    }
                    else if (playerScore == distinctRankedScores[iScoreBoardPosition])
                    {
                        // if scores match then share position
                        refPosition = iScoreBoardPosition + 1;
                    }
                    else
                    {
                        // else go above
                        refPosition = iScoreBoardPosition + 2;
                    }
                }
                else if (playerScore == distinctRankedScores[iScoreBoardPosition] && iScoreBoardPosition < iscoreboardCount)
                {
                    // if no movement we need the same logic as above - share position
                    refPosition = iScoreBoardPosition + 1;
                }
                
                scoreBoardPositions.Add(refPosition);

            }

            return scoreBoardPositions;
        }
    }
}
