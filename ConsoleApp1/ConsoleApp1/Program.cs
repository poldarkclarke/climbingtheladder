﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            
            /*
            Sample Input... 
            
            1) The first line contains an integer , the number of players on the leaderboard.
            2) The next line contains space-separated integers , the leaderboard scores in decreasing order.
            3) The next line contains an integer, , denoting the number games Alice plays.
            4) The last line contains space-separated integers , the game scores.

            Example input
            7
            100 100 50 40 40 20 10
            4
            5 25 50 120
              
             */

            // read inputs
            int rankedCount = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> rankedScores = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(x => Convert.ToInt32(x)).ToList();

            int playerCount = Convert.ToInt32(Console.ReadLine().Trim());

            List<int> playerScores = Console.ReadLine().TrimEnd().Split(' ').ToList().Select(x => Convert.ToInt32(x)).ToList();
            

            /* DEFAULT VALUES easier to test
            int rankedCount = 7;

            List<int> rankedScores = new List<int>() { 100, 100, 50, 40, 40, 20, 10 };

            int playerCount = 4;

            List<int> playerScores = new List<int>() { 5, 25, 50, 120 };
            */


            /* validate inputs

            Constraints

                1 <= n <= 2*10^5 (rankedCount)
                1 <= m <= 2*10^5 (playerCount)
                0 <= scoresi <= 10^9 for 0 <= i < n (rankedScores)
                0 <= alicej <= 10^9 for 0 <= i < m (playerScores)
                
            The existing leaderboard, scores, is in descending order.
            Alice's scores are cumulative, so alice is in ascending order.
            
            Subtask (not sure what this means)
                For  60% of the maximum score:
                1 <= n <= 200
                1 <= m <= 200

            */

            if ( rankedCount >=1 && rankedCount<= 200_000 &&
                 playerCount >= 1 && playerCount <= 200_000 &&
                 rankedScores.Count >= 0 && rankedScores.Count <= 1_000_000_000 &&
                 playerScores.Count >= 0 && playerScores.Count <= 1_000_000_000
                )
            {
                List<int> result = ClimbingTheLadder.ClimbingLeaderboard(rankedScores, playerScores);

                Console.WriteLine(String.Join("\n", result));
            }
                
        }

    }
}
