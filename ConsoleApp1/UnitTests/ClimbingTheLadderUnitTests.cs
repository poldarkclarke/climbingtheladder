using ConsoleApp1;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace UnitTests
{
    public class ClimbingTheLadderUnitTests
    {
        public ClimbingTheLadderUnitTests()
        {
            // nothing to set up here...
        }

        [Fact]
        public void GivenExample_TopScoreReached()
        {
            // arrange 
            List<int> rankedScores = new List<int>() { 100, 100, 50, 40, 40, 20, 10 };
            List<int> playerScores = new List<int>() { 5, 25, 50, 120 };

            var expectedResponse = new List<int>
            {
                6,4,2,1
            };


            // act
            var response = ClimbingTheLadder.ClimbingLeaderboard(rankedScores, playerScores);

            //assert
            response.Should()
                .BeEquivalentTo(expectedResponse);
        }


        [Fact]
        public void GivenExampleTweak_TopScoreNotReached()
        {
            // arrange 
            List<int> rankedScores = new List<int>() { 100, 100, 50, 40, 40, 20, 10 };  // 5 positions
            List<int> playerScores = new List<int>() { 5, 25, 50, 65, 95, 96 };

            var expectedResponse = new List<int>
            {
                6,4,2,2,2,2
            };


            // act
            var response = ClimbingTheLadder.ClimbingLeaderboard(rankedScores, playerScores);

            //assert
            response.Should()
                .BeEquivalentTo(expectedResponse);
        }


        [Fact]
        public void GivenExampleTweak_TopScoreNotReached_mix2()
        {
            // arrange 
            List<int> rankedScores = new List<int>() { 100, 70, 50, 40, 20, 10 };  // 6 positions
            List<int> playerScores = new List<int>() { 5, 25, 50, 65, 75, 95, 96 };

            var expectedResponse = new List<int>
            {
                7,5,3,3,2,2,2
            };

            // act
            var response = ClimbingTheLadder.ClimbingLeaderboard(rankedScores, playerScores);

            //assert
            response.Should()
                .BeEquivalentTo(expectedResponse);
        }

    }
}
